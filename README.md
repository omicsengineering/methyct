## methyCT

methyCT is a tool for imputing cell type specific methylation profiles from bulk (whole blood) dataset.

### Install methyCT package
```
library(devtools);
install_bitbucket(repo="omicsengineering/methyCT")
```

